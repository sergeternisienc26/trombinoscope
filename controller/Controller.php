<?php


//appel les modèles via require_once
require_once('./modele/controllerModel.php');
require_once('./modele/Template.php');
require_once('./modele/apiModel.php');

//appel de l'api racine
$urlApi = "https://www.lecoledunumerique.fr/wp-json/wp/v2/";
$templateEngineDirectory = new viewTemplate('view'); //instenciation 
$request = new apiModel($urlApi);
$controller = new ClassController($templateEngineDirectory, $request);

// $urlPromotion ="https://www.lecoledunumerique.fr/wp-json/wp/v2/Promotion?_fields=id,name,count";
// $promotions = new ApprenticeFetcher($urlPromotion);
// $urlCompetences ="https://www.lecoledunumerique.fr/wp-json/wp/v2/Competence?_fields=id,name,count";
// $competences= new ApprenticeFetcher($urlCompetences);

echo $controller->displayPart('header');
echo $controller->displayList();
echo $controller->displayPart('footer');

/*$fileJsonApiC26 = new ApprenticeFetcher("http://localhost/apic26wp/wp-json/wp/v2/apprenants?_fields=prenom,competences");
$datas = $fileJsonApiC26->getApprenticeData();
echo "<pre>";
var_dump($datas);
echo "</pre>";*/

/*$urlApiComp =new ApprenticeFetcher ("https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?_fields=id,nom,prenom,promotion,competences,linkedin,portfolio,cv");
$datasComp = $urlApiComp->getApprenticeData();
     echo "<pre>";
     var_dump($datasComp);
     echo "</pre>";*/
  

/*$urlApiProm =new ApprenticeFetcher ("http://localhost/apic26wp/wp-json/wp/v2/apprenants?_promotions");
$datasProm = $urlApiProm->getApprenticeData();
     echo "<pre>";
     var_dump($datasProm);
     echo "</pre>";*/



