<?php

class ClassController //création classe
{

    private $templateEngineDirectory;
    private $request;

    public function __construct($templateEngineDirectory, $request)
    {
        $this->templateEngineDirectory = $templateEngineDirectory;
        $this->request = $request;
    }

    public function displayList()
    {
        $data_student       =   $this->request->getApprenticeData($_GET);
        $data_competence    =   $this->request->getCompetData();
        $data_promotion     =   $this->request->getPromoData();
    
        return $this->templateEngineDirectory->render('view.php',
        array(
            'data_student'      =>  $data_student, 
            'data_promotion'    =>  $data_promotion,
            'data_competence'   =>  $data_competence,
        ));
    }


    public function displayPart($part) // $part qui sera ensuite remplacé par header, footer, searchBar
    {
        $header_engine = $this->templateEngineDirectory;
        return $header_engine->render($part . '.php');
    }

}