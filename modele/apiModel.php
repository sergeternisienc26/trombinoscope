<?php 
/*
$url = file_get_contents( "http://app-d6e4214a-9a28-46b5-967f-565f071952c1.cleverapps.io/students");
$fileJson = json_decode($url,true);
var_dump($fileJson);
*/
?>


<?php
/*$curl = curl_init("http://app-d6e4214a-9a28-46b5-967f-565f071952c1.cleverapps.io/students");
$data = curl_exec($curl);
if($data ===false) {
    var_dump(curl_error($curl));
}
else {
    var_dump($data);

}*/
?>

<?php
    class apiModel {
        public $apiC26;
        
        public function __construct($apiC26) 
        {
            $this->apiC26 = $apiC26;
        }
          
        public function getApprenticeData($arg) {
            $data = $this->fetchData('apprenant', $arg);
            return $this->decode($data);
        }

        public function getPromoData() {
            $data = $this->fetchData('promotion');
            return $this->decode($data);
        }

        public function getCompetData() {
            $data = $this->fetchData('competence');
            return $this->decode($data);
        }

        private function fetchData($termination, $arg=null) {
            switch($termination){
                case 'apprenant':
                    $param = 'apprenants?_fields=id,title,excerpt,portfolio,linkedin,cv,image,competences,promotion,nom,prenom&per_page=100';
                    if($arg!=null){
                        if(!empty($arg['promotion'])){
                            $param .='&Promotion='. $arg['promotion'];
                        }
                        if(!empty($arg['search'])){
                            $param .='&search='. $arg['search'];
                        }
                        if(!empty($arg['competence'])){
                            $param .='&Competence='. $arg['competence'];
                        }
                    }
                    break;
                    case 'promotion' : $param='promotion?_fieds=id,name,count';
                    break;
                    case 'competence' : $param='competence?_fieds=id,name,count';
                    break;
            }

            $curl = curl_init($this->apiC26 . $param);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($curl);
            if(!$data) {
                throw new Error("Oh non pas l'api");
            }

            return $data;
        }

        private function decode($jsonString) {
            return json_decode($jsonString);
        }

    }

