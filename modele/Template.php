<?php

class viewTemplate
{

    public $appTemplate;//propriété classe

    function __construct($appTemplate)//constructeur appel methode à chaque instance de la classe
    {
      $this->appTemplate = $appTemplate;
    }

    public function render($template,array $parameters = array())
    /*avec extract tableau associatif 
    crée variables dont les noms sint les index du tableau et affecte valeur associée
    pour chque paire clé/valeur extract() crée une variable avec paramètreds flags et prefix

    */
    {
        extract($parameters);//importe variables tableau parameters

        
        if(false===$this->isAbsolutePath($template)) {
            $template = $this->appTemplate.DIRECTORY_SEPARATOR.$template;//chemin adaptable pour chaque plateforme
        }

        ob_start(); //démarre temporisation de sortie->met en cache les données
        include($template);

        return ob_get_clean();//retourne le contenu du tampon et l'efface
    }

    private function isAbsolutePath($file)
    {
        if (strspn($file,'/\\', 0,1) //trouve longueur segment -> $file à analyser pour reste caractères de 0 à 1
        || (strlen($file) > 3 && ctype_alpha($file[0]) //calcul taille file sup. 3 et vérif 1er du tableau file est alphanétique
        && substr($file, 1, 1 ) === ':' //retourne caractère de file position 1
        && (strspn($file, '/\\', 2, 1)) //trouve longueur segment -> $file à analyser pour reste caractères de 2 à 1
        )
        || null !==parse_url($file, PHP_URL_SCHEME)//analyse url et retourne protocole (schéma)
        ) {
            return true;
        }
        return false;
    }
}
