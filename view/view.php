    <section class="recherche">

        <h3>Retrouvez nos apprenants parmi nos superbes formations</h3>

        <form method="get">

            <div class="searchbar">

                <div class="promotions">
                    <label for="promotions">Promotions</label>
                    <select name="promotion">
                        <option value=""> -- Choix -- </option>
                        <?php foreach ($data_promotion as $promo) : ?>
                            <option value="<?php echo ($promo->id); ?>"><?php echo ($promo->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="searchbarButton">
                    <span>Rechercher</span>
                    <input type="text" name="search">
                </div>

                <div class="competence">
                    <label for="competence">Competences  </label>
                    <select name="competence">
                        <option value=""> -- Choix -- </option>
                        <?php foreach ($data_competence as $competence) : ?>
                            <option value="<?php echo ($competence->id); ?>"><?php echo ($competence->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input type="submit" value="Envoyer">
            </div>

        </form>

    </section>

    <section class="contentApprenant">

        <div class="containerApprenant">
            <?php foreach ($data_student as $apprenant) : ?>
                <div class="cardApprenant">
                    <div class="face01">
                        <div class="apprenant">
                            <div class="imgApprenant">
                                <img src="<?php echo ($apprenant->image); ?>" alt="">
                            </div>
                            <div class="basCardFace01">
                                <div class="coordoneesApprenant">
                                    <p><?php echo ($apprenant->nom); ?></p>
                                    <P><?php echo ($apprenant->prenom); ?></P>
                                </div>

                                <div class="competences">
                                    <?php foreach ($apprenant->competences as $competences) : ?>
                                        <p> <?php echo ($competences->name); ?></p>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="face02">
                        <div class="promotions">
                            <p><?php echo ($apprenant->promotion->name); ?></p>
                        </div>
                        <div class="textApprenant">
                            <p><?php echo ($apprenant->excerpt->rendered); ?></p>
                        </div>
                        <div class="liens">
                            <a href="<?php echo ($apprenant->lien_linkendin); ?>" target="_blank">Linkendin </a>
                            <a href="<?php echo ($apprenant->portfolio); ?>" target="_blank">Portfolio</a>
                            <a href="<?php echo ($apprenant->cv); ?> " target="_blank">CV</a>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
    </section>